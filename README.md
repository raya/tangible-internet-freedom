# Tangible Internet Freedom

A set of playful exercises that digital security and Tor trainers can make use of in training sessions to aid in the teaching of technical concepts related to Internet freedom.

## Name
Tangible Internet Freedom

## Description
In trainings it would be useful to have hands-on exercises that are actually "hands on". The idea is to explain and illustrate technical concepts related to Internet freedom topics via tangible exercises that include 3D printed objects, laser cut objects, paper material, etc.

## Support
For help, feel free to ping the team on irc: #hw22-tangible

## Contributing
This project is open for contributions, be it:
- providing feedback on activities
- suggesting new activities or variations of activities

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Need to add license.

## Project status
This project was started as part of [Hackweek 2022](https://gitlab.torproject.org/tpo/community/hackweek).

## Additional resources
* Jon Camfield wrote an explanation of PGP and public keys using a hotel room analogy: https://www.joncamfield.com/blog/2017.01/lets-talk-about-pgp